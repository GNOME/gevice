--Table: device

--DROP TABLE device;

CREATE TABLE device (
  id_dev  varchar(30) PRIMARY KEY NOT NULL UNIQUE,
  ip_dev  varchar(15) DEFAULT '127.0.0.1',
  obs_dev varchar(200) DEFAULT '-'
);

--Table: connect

--DROP TABLE connect;

CREATE TABLE connect (
  id_devp  varchar(30) NOT NULL,
  id_devc  varchar(30) NOT NULL,
  PRIMARY KEY (id_devp, id_devc),
  /* Foreign keys */
  FOREIGN KEY (id_devc)
    REFERENCES device(id_dev)
    ON DELETE CASCADE
    ON UPDATE CASCADE, 
  FOREIGN KEY (id_devp)
    REFERENCES device(id_dev)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
