#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
import config
import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class GeviceModel:
  def __init__ (self,gevice):
    self.treestore = Gtk.TreeStore(str,str,str)

    self.treeview = Gtk.TreeView (model=self.treestore)
    self.treeview.set_rules_hint (True)
    self.treeview.set_reorderable (True)

    # create columns
    col = Gtk.TreeViewColumn (_("Device"))
    col.set_resizable (True)
    self.treeview.append_column (col)
    cell =  Gtk.CellRendererText()
    col.pack_start (cell,False)
    col.add_attribute (cell,"text",0)
    
    col = Gtk.TreeViewColumn (_("IP Address"))
    col.set_resizable (True)
    self.treeview.append_column (col)
    cell =  Gtk.CellRendererText()
    col.pack_start (cell,False)
    col.add_attribute (cell,"text",1)

    col = Gtk.TreeViewColumn (_("Comments"))
    col.set_resizable (True)
    self.treeview.append_column (col)
    cell =  Gtk.CellRendererText()
    col.pack_start (cell,False)
    col.add_attribute (cell,"text",2)

    selection = self.treeview.get_selection()
    selection.connect ("changed",gevice.on_device_selection_changed)

  def insert_new_item (self,newiter,name,ip,com):
    self.treestore.set_value (newiter,0,str(name))
    self.treestore.set_value (newiter,1,str(ip))
    self.treestore.set_value (newiter,2,str(com))

  def get_data_of_device_from_model(self,selected):
    data = selected["model"].get(selected["iter"],0,1,2)
    return data
