#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gdk
import os

import config

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class GeviceFind:
  def __init__ (self):
    self.window_find = None
    self.button_find_cancel = None
    self.button_find_find = None
    self.entry_find = None
    self.radiobutton_namedevice = None
    self.radiobutton_ip = None
    self.data_finded = []

  def load_interface (self,gevice):
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join (config.UIDIR, "find.xml"))

    self.window_find = builder.get_object ("window_find")
    self.button_find_cancel = builder.get_object ("button_find_cancel")
    self.button_find_find = builder.get_object ("button_find_find")
    self.entry_find = builder.get_object ("entry_find")
    self.radiobutton_namedevice = builder.get_object ("radiobutton_namedevice")
    self.radiobutton_ip = builder.get_object ("radiobutton_ip")

    self.window_find.connect ("delete-event",self.on_window_find_delete_event)
    self.button_find_cancel.connect ("clicked",self.on_button_cancel_find_clicked)
    self.button_find_find.connect ("clicked",self.on_button_find_find_clicked,gevice)
    self.entry_find.connect ("key-press-event", self.on_entry_find_key_press,gevice)
    self.radiobutton_namedevice.connect ("toggled", self.on_radiobutton_toggled)
    self.radiobutton_ip.connect ("toggled", self.on_radiobutton_toggled)

    self.entry_find.grab_focus()

  def show_interface (self):
    self.window_find.show_all()    
  
  def on_radiobutton_toggled(self,radiobutton):
    self.entry_find.grab_focus()    

  def close_window (self,window):
    window.destroy()

  def on_button_cancel_find_clicked (self,button):
    self.close_window (self.window_find)

  def on_window_find_delete_event (self,window,event):
    self.close_window (window)

  def on_button_find_find_clicked(self,button,gevice):
    self.sname = self.entry_find.get_text().strip()

    if (self.radiobutton_namedevice.get_active() == True):
      self.column_find = 0
    elif (self.radiobutton_ip.get_active() == True):
      self.column_find = 1

    gevice.gmodel.treestore.foreach(self.search_in_iter,gevice)

  def search_in_iter (self,model,path,iter,gevice):    
    data = model.get_value(iter,self.column_find)
    res = data.find(self.sname)

    if (res >= 0):
      try:
        # exist in list?
        self.data_finded.index(data)
      except:
        # add to list of match
        self.data_finded.append(data)
        selection = gevice.gmodel.treeview.get_selection()
        gevice.gmodel.treeview.set_cursor(path,None,False)
        gevice.gmodel.treeview.grab_focus()
        selection.select_iter(iter)
        return True

  def on_entry_find_key_press (self,widget,event,gevice):
    if Gdk.keyval_name(event.keyval) == "Return":
      self.on_button_find_find_clicked(None,gevice)
