#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
import os
from pygraphviz import *

import config

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class GeviceDiagram:
  def __init__ (self):
    self.window_diagram = None
    self.button_diagram_cancel = None
    self.button_diagram_generate = None
    self.combobox_program = None
    self.combobox_format = None
    self.iterfirst = None
    self.default_tmp = "/tmp/"
    self.color_node = "black"
    self.fontsize = 8
    self.shape = 'box'

    self.A = None
    self.form = None
    self.prog = None

    self.name = None
    self.ip = None

  def load_interface (self,gevice):
    builder = Gtk.Builder()
    builder.add_from_file(os.path.join (config.UIDIR, "diagram.xml"))

    self.window_diagram = builder.get_object ("window_diagram")
    self.button_diagram_cancel = builder.get_object ("button_diagram_cancel")
    self.button_diagram_generate = builder.get_object ("button_diagram_generate")
    self.combobox_program = builder.get_object ("combobox_program")
    self.combobox_format = builder.get_object ("combobox_format")

    # set default value for diagram
    self.combobox_format.set_active (0)
    self.combobox_program.set_active (0)

    self.window_diagram.connect ("delete-event",self.on_window_diagram_delete_event)
    self.button_diagram_cancel.connect ("clicked",self.on_button_diagram_cancel_clicked)
    self.button_diagram_generate.connect ("clicked",self.on_button_diagram_generate_clicked,gevice)

  def show_interface (self):
    self.window_diagram.show_all()

  def close_window (self,window):
    window.destroy()

  def on_button_diagram_cancel_clicked (self,button):
    self.close_window(self.window_diagram)

  def on_window_diagram_delete_event (self,window,event):
    self.close_window(window)

  def on_button_diagram_generate_clicked (self,button,gevice):
    self.A = AGraph()

    # attributes of nodes
    self.A.node_attr['shape'] = self.shape

    iter_form = self.combobox_format.get_active_iter()
    model_form = self.combobox_format.get_model()
    self.form = model_form.get_value(iter_form,0)
    
    iter_prog = self.combobox_program.get_active_iter()
    model_prog = self.combobox_program.get_model()
    self.prog = model_prog.get_value(iter_prog,0)

    i = gevice.get_iter_selected(gevice.gmodel.treeview)

    self.iterfirst = i["iter"]
    self.print_child (gevice,i["iter"],None)

    file = self.default_tmp + "tmp_gevice_diagram" + "." + self.form
    self.A.draw(file,format=self.form,prog=self.prog) 

    # show diagram with eog (FIXME: call to default viewer)
    id = os.spawnlp(os.P_NOWAIT,'eog','eog',file)

    self.close_window(self.window_diagram)

  def get_data_of_device (self,iter,gevice):
    self.name,self.ip = gevice.gmodel.treestore.get(iter,0,1)

    if (gevice.gpref.gsettings.get_boolean("diagip")):          
      self.name = self.name + '\\n(' + self.ip + ')'

  def print_child(self,gevice,parent_iter,parent_name):
    while (parent_iter):
      self.get_data_of_device (parent_iter,gevice)
      self.join_nodes (parent_name,self.name,gevice)

      if gevice.gmodel.treestore.iter_has_child(parent_iter):
        child = gevice.gmodel.treestore.iter_children(parent_iter)
        self.print_child (gevice,child,self.name)

      if (not self.iterfirst == parent_iter):
        next_iter = gevice.gmodel.treestore.iter_next(parent_iter)
        parent_iter = next_iter
      else:
        return

  def join_nodes (self,parent,child,gevice):
    self.A.add_node (child,color=self.color_node,fontsize=self.fontsize)
    if parent:
      self.A.add_edge(parent,child)
