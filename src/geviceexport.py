#!/usr/bin/env python
#
# gevice GNOME Network Device Manager.
# Copyright (C) Alejandro Valdes Jimenez 2008 <avaldes@gnome.org>
# 
# gevice.py is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gevice.py is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
import config

import gettext
gettext.bindtextdomain(config.PACKAGE,config.LOCALEDIR)
gettext.textdomain(config.PACKAGE)
gettext.bind_textdomain_codeset (config.PACKAGE, "UTF-8");
from gettext import gettext as _

class GeviceExport:
  def __init__ (self,tree=None,separator="|",filename="gevice",extension=".csv"):
      self.__extension = extension
      self.__tree = tree
      self.__separator = separator
      self.__endline = "\n"
      self.__filename = filename + extension
      self.__status = False
      self.__FILE = None
      self.__write_to_file()

  def __write_to_file (self):
    self.__FILE = open (self.__filename,"w")
    self.__walking_tree ()
    self.__FILE.close()
    self.__status = True

  def __walking_tree(self):
    for n in self.__tree.children:
      line = n.name + self.__separator
      line = line + n.ip + self.__separator
      line = line + n.com
      self.__write_line(line)
      self.__walking_children(n)

  def __walking_children(self,node):
    for n in node.children:
      line = n.name + self.__separator
      line = line + n.ip + self.__separator
      line = line + n.com
      self.__write_line(line)
      self.__walking_children(n)

  def __write_line(self,line):
    self.__FILE.write (line + self.__endline)

  def get_status(self):
    return self.__status
